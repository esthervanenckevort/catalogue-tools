/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfiguration.class)
public class PostcodeServiceTest {

    @Autowired
    PostcodeService service;

    /**
     * Test of getCoordinates method, of class PostcodeService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCoordinates() throws Exception {
        NLPostalcode code = new NLPostalcode("9713GZ");
        Optional<WGS84Coordinate> coordinates = service.getCoordinates(code);
        Assert.assertNotNull("Result should not be null", coordinates);
        Assert.assertTrue(coordinates.isPresent());
        Assert.assertNotNull(coordinates.get());
        WGS84Coordinate wgs84 = coordinates.get();
        Assert.assertTrue(wgs84.getLatitude() > wgs84.getLongitude());
    }

}

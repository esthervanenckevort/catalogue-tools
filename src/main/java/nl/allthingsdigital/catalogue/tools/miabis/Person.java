/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.allthingsdigital.catalogue.tools.miabis;

import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Country;
import java.util.Objects;

/**
 *
 * @author david
 */
public class Person extends AbstractEntity {
    private static final String FIELD_ID = "id";
    private static final String FIELD_FIRST_NAME = "first_name";
    private static final String FIELD_LAST_NAME = "last_name";
    private static final String FIELD_PHONE = "phone";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_ADDRESS = "address";
    private static final String FIELD_ZIP = "zip";
    private static final String FIELD_CITY = "city";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_DEPARTMENT = "department";
    private static final String FIELD_ORGANISATION = "juristic_person";
    private static final String FIELD_ORCID = "orcid";
    private static final String[] FIELDS = {
        FIELD_ID, FIELD_FIRST_NAME, FIELD_LAST_NAME, FIELD_PHONE, FIELD_EMAIL,
        FIELD_ADDRESS, FIELD_ZIP, FIELD_CITY, FIELD_COUNTRY, FIELD_DEPARTMENT,
        FIELD_ORGANISATION, FIELD_ORCID};

    private String id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String address;
    private String zip;
    private String city;
    private Country country;
    private String department;
    private Organisation organisation;
    private String orcid;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.department, other.department)) {
            return false;
        }
        return true;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return id;
            case FIELD_FIRST_NAME:
                return firstName != null ? firstName : "";
            case FIELD_LAST_NAME:
                return lastName != null ? lastName : "";
            case FIELD_PHONE:
                return phone != null ? phone : "";
            case FIELD_EMAIL:
                return email != null ? email : "";
            case FIELD_ADDRESS:
                return address != null ? address : "";
            case FIELD_ZIP:
                return zip != null ? zip : "";
            case FIELD_CITY:
                return city != null ? city : "";
            case FIELD_COUNTRY:
                return country != null ? country.getId() : "";
            case FIELD_DEPARTMENT:
                return department != null ? department : "";
            case FIELD_ORGANISATION:
                return organisation != null ? organisation.getId() : "";
            case FIELD_ORCID:
                return orcid != null ? orcid : "";
            default:
                throw new RuntimeException("illegal field");
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.firstName);
        hash = 29 * hash + Objects.hashCode(this.lastName);
        hash = 29 * hash + Objects.hashCode(this.phone);
        hash = 29 * hash + Objects.hashCode(this.email);
        hash = 29 * hash + Objects.hashCode(this.department);
        return hash;
    }

    @Override
    public String toString() {
        return id != null ? id : "";
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }
}

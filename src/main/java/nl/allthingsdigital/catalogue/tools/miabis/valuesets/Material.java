/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis.valuesets;

/**
 *
 * @author david
 */
public enum Material {

    DNA("biobankMaterialStoredDNA"),
    CDNA("biobankMaterialStoredcDNAmRNA"),
    MICRO_RNA("biobankMaterialStoredmicroRNA"),
    WHOLE_BLOOD("biobankMaterialStoredWholeBlood"),
    PERIPHERAL_BLOOD_CELLS("biobankMaterialStoredPBC"),
    PLASMA("biobankMaterialStoredPlasma"),
    SERUM("biobankMaterialStoredSerum"),
    TISSUE_FROZEN("biobankMaterialStoredTissueCryo"),
    TISSUE_PARAFFIN_EMBEDDED("biobankMaterialStoredTissueParaffin"),
    CELL_LINES("biobankMaterialStoredCellLines"),
    URINE("biobankMaterialStoredUrine"),
    SALIVA("biobankMaterialStoredSaliva"),
    FECES("biobankMaterialStoredFaeces"),
    PATHOGEN("biobankMaterialStoredPathogen"),
    OTHER("biobankMaterialStoredOther"),
    NAV(null);

    private final String attr;

    private Material(String attr) {
        this.attr = attr;
    }

    public String getAttributeName() {
        return attr;
    }
}

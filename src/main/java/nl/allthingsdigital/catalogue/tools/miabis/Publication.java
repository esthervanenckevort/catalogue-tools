/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis;

import java.net.URL;
import java.util.Objects;

/**
 *
 * @author david
 */
public final class Publication extends AbstractEntity {

    private static final String FIELD_ID = "id";
    private static final String FIELD_PID = "website";
    private static final String FIELD_DESCRIPTION = "description";

    private static final String[] FIELDS = {FIELD_ID, FIELD_PID, FIELD_DESCRIPTION};
    private URL pid;
    private String descripton;
    private String id;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Publication other = (Publication) obj;
        return Objects.equals(this.id, other.id);
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return id;
            case FIELD_PID:
                return pid != null ? pid.toString() : "";
            case FIELD_DESCRIPTION:
                return descripton;
            default:
                throw new RuntimeException("illegal field");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public URL getPid() {
        return pid;
    }

    public void setPid(URL pid) {
        this.pid = pid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(id);
        return hash;
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }

    @Override
    public String toString() {
        return id;
    }
}

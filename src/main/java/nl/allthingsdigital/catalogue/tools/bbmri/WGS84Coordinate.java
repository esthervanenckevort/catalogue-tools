/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
public final class WGS84Coordinate {
    private final double latitude;
    private final double longitude;

    public WGS84Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}

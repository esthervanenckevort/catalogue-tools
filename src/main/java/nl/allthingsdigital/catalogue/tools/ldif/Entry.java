/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.ldif;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.collections4.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
public class Entry {

    private final MultiValueMap<String, String> attributes;
    private final Set<String> objectClasses;
    private final String dn;

    public Entry(final String dnValue, final String... classes) {
        objectClasses = new HashSet<>(Arrays.asList(classes));
        attributes = new MultiValueMap<>();
        dn = dnValue;
    }

    public void add(final String attribute, final String value) {
        if (StringUtils.isNotBlank(attribute)
                && StringUtils.isNotBlank(value)) {
            attributes.put(attribute, value);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("dn: ");
        sb.append(dn);
        sb.append('\n');
        objectClasses.forEach(o -> {
            sb.append("objectClass: ");
            sb.append(o);
            sb.append('\n');
        });
        attributes.keySet().forEach((k) -> {
            attributes.getCollection(k).forEach(a -> {
                sb.append(k);
                sb.append(": ");
                sb.append(a);
                sb.append('\n');
            });
        });
        sb.append('\n');
        return sb.toString();
    }
}

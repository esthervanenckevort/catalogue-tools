package nl.allthingsdigital.catalogue.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import nl.allthingsdigital.catalogue.tools.bbmri.BbmriService;
import nl.allthingsdigital.catalogue.tools.bbmri.EMXRepository;
import nl.allthingsdigital.catalogue.tools.bbmri.LDIFRepository;
import nl.allthingsdigital.catalogue.tools.miabis.SampleCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

    /**
     * Logger instance to log messages for this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    @Autowired
    BbmriService bbmriService;

    @Autowired
    EMXRepository emxRepository;

    @Autowired
    LDIFRepository ldifRepository;

    @Value("${location:./target/}")
    private String location;
    private static final String LDAP = ".ldif";
    private static final String EXCEL = ".xlsx";
    private static final String BASE = "bbmri-nl-";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        final Path workingDir = Paths.get(location);
        if (!workingDir.toFile().isDirectory()) {
            LOGGER.error("{} is not a directory", workingDir);
            throw new RuntimeException("Not a directory.");
        }
        final Path emx = Files.createTempFile(workingDir, BASE, EXCEL);
        final Path ldif = Files.createTempFile(workingDir, BASE, LDAP);
        LOGGER.info("Using temporary files {} {}.", emx, ldif);
        List<SampleCollection> collections = bbmriService.getSampleCollections();
        emxRepository.write(collections, emx);
        ldifRepository.write(collections, ldif);
    }
}
